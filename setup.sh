apt update && apt -y upgrade;

apt install curl tmux git build-essential mariadb-server mariadb-client phpmyadmin python3-pip xclip nmap chromium-browser default-jdk fopenjdk-8-jdk software-properties-common keepassxc -y
apt install apache2 php php-cli php-fpm php-json php-common php-mysql php-zip php-gd php-mbstring php-curl php-xml php-pear php-bcmath -y
apt install ubuntu-restricted-extras ubuntu-restricted-addons

echo 'set -g default-terminal "screen-256color"' >> ~/.tmux.conf
snap install go --classic

# Composer
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
# Symfony Cli
wget https://get.symfony.com/cli/installer -O - | bash 


# VM 30GB+ just with sosumi
sudo snap install sosumi --edge && sosumi || sudo usermod -aG kvm $USER
apt install virtualbox virtualbox—ext–pack

# Alias
alias ..='cd ..'
alias ...='cd ../../'
alias clipboard='xclip -selection clipboard'
alias ltr='ls -lstr'
alias phpc='php bin/console'

# Gnome minimize app when clicked again
gsettings set org.gnome.shell.extensions.dash-to-dock click-action 'minimize'

echo "add noatime to fstab"

# VPN
cd /tmp
wget https://repo.nordvpn.com/deb/nordvpn/debian/pool/main/nordvpn-release_1.0.0_all.deb
sudo dpkg -i nordvpn-release_1.0.0_all.deb 
apt update
apt install nordvpn -y


# Insomnia
# Add to sources
echo "deb https://dl.bintray.com/getinsomnia/Insomnia /" \
    | sudo tee -a /etc/apt/sources.list.d/insomnia.list
wget --quiet -O - https://insomnia.rest/keys/debian-public.key.asc \
    | sudo apt-key add -
sudo apt-get update
sudo apt-get install insomnia -y
